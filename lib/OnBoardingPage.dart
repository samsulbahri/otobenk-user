import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:otobenk/LoginPage.dart';
import 'package:otobenk/model/OnboardingModel.dart';
import 'package:otobenk/util/ColorPallete.dart';

void main() {
  runApp(OnboardingPage());
}

class OnboardingPage extends StatefulWidget {
  @override
  _OnboardingPageState createState() => _OnboardingPageState();
}

class _OnboardingPageState extends State<OnboardingPage> {
  final List<OnboardingModel> introList = [
    OnboardingModel(
      image: "assets/images/Onboarding1.png",
      title: "Cari Bengkel Terdekat",
      description:
          "Tidak perlu takut mogok di jalan cari bengkel disekitar anda dan pesan",
    ),
    OnboardingModel(
      image: "assets/images/Onboarding2.png",
      title: "Servis Motor",
      description: "Sekarang servis motor bisa dirumah aja dengan biaya murah",
    ),
    OnboardingModel(
      image: "assets/images/Onboarding3.png",
      title: "Layanan terbaik",
      description: "Terdapat banyak layanan sesuai dengan kebutuhan anda",
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Swiper.children(
        index: 0,
        autoplay: false,
        loop: false,
        pagination: SwiperPagination(
          margin: EdgeInsets.only(bottom: 20.0),
          builder: DotSwiperPaginationBuilder(
            color: ColorPallete.dotColor,
            activeColor: ColorPallete.dotActiveColor,
            size: 10.0,
            activeSize: 10.0,
          ),
        ),
        control: SwiperControl(
          iconNext: null,
          iconPrevious: null,
        ),
        children: _buildpage(context),
      ),
    );
  }

  List<Widget> _buildpage(BuildContext context) {
    List<Widget> widgets = [];
    for (var i = 0; i < introList.length; i++) {
      OnboardingModel onboardingModel = introList[i];
      if (i == 2) {
        widgets.add(
          Container(
            padding: EdgeInsets.only(
              top: MediaQuery.of(context).size.height / 6,
            ),
            child: ListView(
              children: <Widget>[
                Image.asset(
                  onboardingModel.image,
                  height: MediaQuery.of(context).size.height / 3.5,
                ),
                Padding(
                  padding: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height / 12.0,
                  ),
                ),
                Center(
                  child: Text(
                    onboardingModel.title,
                    style: TextStyle(
                      color: ColorPallete.titleColor,
                      fontSize: 24.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height / 20.0,
                  ),
                ),
                Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: MediaQuery.of(context).size.height / 20.0,
                  ),
                  child: Text(
                    onboardingModel.description,
                    style: TextStyle(
                      color: ColorPallete.descriptionColor,
                      fontSize: 14.0,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 23.0),
                  width: 48.0,
                  padding: const EdgeInsets.all(20.0),
                  child: SizedBox(
                    width: 10.0,
                    height: 50.0,
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(25.0),
                          side: BorderSide(
                              color: Color.fromRGBO(47, 214, 250, 10))),
                      child: const Text(
                        "Selanjutnya",
                        // style: TextStyle(color: Color.fromRGBO(47, 214, 250, 10)),
                        style: TextStyle(
                            color: Color.fromRGBO(252, 252, 252, 10),
                            fontFamily: 'RadikalMedium',
                            fontSize: 16),
                      ),
                      color: Color.fromRGBO(47, 214, 250, 10),
                      // elevation: 4.0,
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => LoginPage()));
                      },
                    ),
                  ),
                )
              ],
            ),
          ),
        );
      } else {
        widgets.add(
          Container(
            padding: EdgeInsets.only(
              top: MediaQuery.of(context).size.height / 6,
            ),
            child: ListView(
              children: <Widget>[
                Image.asset(
                  onboardingModel.image,
                  height: MediaQuery.of(context).size.height / 3.5,
                ),
                Padding(
                  padding: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height / 12.0,
                  ),
                ),
                Center(
                  child: Text(
                    onboardingModel.title,
                    style: TextStyle(
                      color: ColorPallete.titleColor,
                      fontSize: 24.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height / 20.0,
                  ),
                ),
                Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: MediaQuery.of(context).size.height / 20.0,
                  ),
                  child: Text(
                    onboardingModel.description,
                    style: TextStyle(
                      color: ColorPallete.descriptionColor,
                      fontSize: 14.0,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              ],
            ),
          ),
        );
      }
    }
    return widgets;
  }
}
