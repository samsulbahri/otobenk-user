import 'package:flutter/material.dart';
import 'package:otobenk/LoginPage.dart';

class RegisterPage extends StatelessWidget {
  final _formKey = new GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        // padding: const EdgeInsets.symmetric(horizontal: 43.0),
        // padding: const EdgeInsets.only(left: 43.0, right: 43.0, top: 100.0),
        margin:
            EdgeInsets.only(top: 10.0, bottom: 50.0, right: 16.0, left: 16.0),
        child: Form(
          child: Container(
            alignment: Alignment.center,
            child: SingleChildScrollView(
              child: Column(
                // mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Image.asset(
                    'assets/images/logo_otobenk.png',
                    width: 100,
                  ),
                  SizedBox(
                    height: 50,
                  ),
                  Text(
                    "Buat Akun Baru",
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.w700),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  _name(),
                  SizedBox(
                    height: 20,
                  ),
                  _email(),
                  SizedBox(
                    height: 20,
                  ),
                  _password(),
                  SizedBox(
                    height: 30,
                  ),
                  _daftarButton(context),
                  SizedBox(
                    height: 10,
                  ),
                  Text("Sudah Punya Akun?"),
                  SizedBox(
                    height: 5,
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => LoginPage()));
                    },
                    child: new Text(
                      "Klik Disini",
                      style: TextStyle(color: Color.fromRGBO(47, 214, 250, 10)),
                    ),
                  )
                ],
              ),
            ),
          ),
          key: _formKey,
        ),
      ),
    );
  }

  InputDecoration _buildInputDecoration(String hint, String iconPath) {
    return InputDecoration(
        focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Color.fromRGBO(0, 0, 0, 1))),
        hintText: hint,
        enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Color.fromRGBO(151, 151, 151, 1))),
        hintStyle: TextStyle(color: Color.fromRGBO(0, 0, 0, 1)),
        icon: iconPath != ''
            ? Image.asset(
                iconPath,
                width: 20,
                color: Color.fromRGBO(0, 0, 0, 10),
              )
            : null,
        errorStyle: TextStyle(color: Color.fromRGBO(248, 218, 87, 1)),
        errorBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Color.fromRGBO(248, 218, 87, 1))),
        focusedErrorBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Color.fromRGBO(248, 218, 87, 1))));
  }

  Widget _name() {
    return TextFormField(
      validator: (value) =>
          value.isEmpty ? "Nama anda tidak boleh dikosongkan" : null,
      style: TextStyle(
          color: Color.fromRGBO(0, 0, 0, 1), fontFamily: 'RadikalLight'),
      decoration:
          _buildInputDecoration("Nama Lengkap", 'assets/icons/ic_avatar.png'),
    );
  }

  Widget _email() {
    return TextFormField(
      validator: (value) =>
          isEmail(value) ? "Maaf, kami tidak mengenali email anda." : null,
      style: TextStyle(
          color: Color.fromRGBO(0, 0, 0, 1), fontFamily: 'RadikalLight'),
      decoration:
          _buildInputDecoration("Email", 'assets/icons/ic_paper-plane.png'),
    );
  }

  Widget _password() {
    return TextFormField(
      validator: (value) =>
          value.isEmpty ? "Password anda tidak boleh dikosongkan" : null,
      style: TextStyle(
          color: Color.fromRGBO(0, 0, 0, 1), fontFamily: 'RadikalLight'),
      decoration: _buildInputDecoration("Password", 'assets/icons/ic_link.png'),
    );
  }

  Widget _daftarButton(BuildContext context) {
    return Container(
      // margin: EdgeInsets.only(top: 43.0),
      width: MediaQuery.of(context).size.width * 0.62,
      child: SizedBox(
        width: 10.0,
        height: 50.0,
        child: RaisedButton(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(25.0),
              side: BorderSide(color: Color.fromRGBO(47, 214, 250, 10))),
          child: const Text(
            "Buat Akun",
            // style: TextStyle(color: Color.fromRGBO(47, 214, 250, 10)),
            style: TextStyle(
                color: Color.fromRGBO(252, 252, 252, 10),
                fontFamily: 'RadikalMedium',
                fontSize: 16),
          ),
          color: Color.fromRGBO(47, 214, 250, 10),
          // elevation: 4.0,
          onPressed: () {},
        ),
      ),
    );
  }

  bool isEmail(String value) {
    String regex =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';

    RegExp regExp = new RegExp(regex);
    return value.isNotEmpty && regExp.hasMatch(value);
  }
}
