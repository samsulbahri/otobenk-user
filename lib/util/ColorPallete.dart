import 'package:flutter/material.dart';

class ColorPallete {
  static const dotColor = Color(0xfff2f2f2);
  static const dotActiveColor = Color(0xff2fd6fa);
  static const titleColor = Color(0xff000000);
  static const descriptionColor = Color(0xff000000);
}
