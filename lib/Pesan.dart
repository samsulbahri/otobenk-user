import 'package:flutter/material.dart';
import 'package:otobenk/LoginPage.dart';

class Pesan extends StatefulWidget {
  @override
  _PesanState createState() => _PesanState();
}

class _PesanState extends State<Pesan> {
  bool mesin = false;
  bool gardan = false;
  bool lampu = false;
  String dropDownValue = 'Service Ringan';

  void onChange(bool value) {
    setState(() {
      mesin = value;
    });
    setState(() {
      gardan = value;
    });
    setState(() {
      lampu = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        decoration: BoxDecoration(
            gradient: LinearGradient(begin: Alignment.topCenter, colors: [
          Colors.blue[100],
          Colors.blue[200],
          Colors.blue[300],
        ])),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(
              height: 20,
            ),
            Padding(
              padding: EdgeInsets.all(20),
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      iconBack(),
                      SizedBox(
                        height: 5,
                      ),
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      txtHeader(),
                    ],
                  ),
                ],
              ),
            ),
            Expanded(
              child: Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(40),
                        topRight: Radius.circular(40))),
                child: Padding(
                  padding: EdgeInsets.all(15),
                  child: Column(
                    children: <Widget>[
                      SizedBox(
                        height: 10,
                      ),
                      txtPaket(),
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        padding: EdgeInsets.only(
                            top: 2, left: 20, right: 2, bottom: 2),
                        decoration: BoxDecoration(
                          color: Colors.grey[100],
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Column(
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                DropdownButton<String>(
                                    value: dropDownValue,
                                    elevation: 16,
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 12,
                                        fontWeight: FontWeight.w600),
                                    onChanged: (String newValue) {
                                      setState(() {
                                        dropDownValue = newValue;
                                      });
                                    },
                                    items: <String>[
                                      "Service Ringan",
                                      "Service Berat"
                                    ].map<DropdownMenuItem<String>>(
                                        (String value) {
                                      return DropdownMenuItem<String>(
                                        value: value,
                                        child: Text(value),
                                      );
                                    }).toList()),
                              ],
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      txtService(),
                      SizedBox(
                        height: 5,
                      ),
                      Container(
                        padding: EdgeInsets.all(2),
                        decoration: BoxDecoration(
                          color: Colors.grey[100],
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Column(
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Checkbox(
                                  value: mesin,
                                  onChanged: (bool value) {
                                    onChange(value);
                                  },
                                ),
                                Text(
                                  'Penggantian Oli Mesin',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 12,
                                      fontWeight: FontWeight.w600),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 6,
                      ),
                      Container(
                        padding: EdgeInsets.all(2),
                        decoration: BoxDecoration(
                          color: Colors.grey[100],
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Column(
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Checkbox(
                                  value: gardan,
                                  onChanged: (bool value) {
                                    onChange(value);
                                  },
                                ),
                                Text(
                                  'Penggantian Oli Gardan',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 12,
                                      fontWeight: FontWeight.w600),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 6,
                      ),
                      Container(
                        padding: EdgeInsets.all(2),
                        decoration: BoxDecoration(
                          color: Colors.grey[100],
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Column(
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Checkbox(
                                  value: lampu,
                                  onChanged: (bool value) {
                                    onChange(value);
                                  },
                                ),
                                Text(
                                  'Pengecekan lampu & sistem kelistrikan',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 12,
                                      fontWeight: FontWeight.w600),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      txtRekomendasi(),
                      SizedBox(
                        height: 5,
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          color: Colors.grey[100],
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Column(
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                namaToko(),
                                SizedBox(
                                  height: 20,
                                ),
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                alamatToko(),
                                SizedBox(
                                  height: 10,
                                ),
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                jarakToko(),
                                SizedBox(
                                  height: 30,
                                ),
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                linkMapToko(),
                                SizedBox(
                                  height: 20,
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

Widget iconBack() {
  return Padding(
    padding: const EdgeInsets.only(left: 5),
    child: Icon(Icons.arrow_back),
  );
}

Widget txtHeader() {
  return Padding(
    padding: const EdgeInsets.only(left: 5),
    child: Align(
      alignment: Alignment.centerLeft,
      child: Text(
        "Buat Pesanan",
        style: TextStyle(
            color: Colors.black, fontSize: 18, fontWeight: FontWeight.w600),
      ),
    ),
  );
}

Widget txtPaket() {
  return Padding(
    padding: const EdgeInsets.only(left: 5),
    child: Align(
      alignment: Alignment.centerLeft,
      child: Text(
        "Pilih Paket Servis",
        style: TextStyle(
            color: Colors.black, fontSize: 15, fontWeight: FontWeight.w600),
      ),
    ),
  );
}

Widget txtService() {
  return Padding(
    padding: const EdgeInsets.only(left: 5),
    child: Align(
      alignment: Alignment.centerLeft,
      child: Text(
        "Pilih Servis",
        style: TextStyle(
            color: Colors.black, fontSize: 15, fontWeight: FontWeight.w600),
      ),
    ),
  );
}

Widget txtRekomendasi() {
  return Padding(
    padding: const EdgeInsets.only(left: 5),
    child: Align(
      alignment: Alignment.centerLeft,
      child: Text(
        "Bengkel Rekomendasi",
        style: TextStyle(
            color: Colors.black, fontSize: 15, fontWeight: FontWeight.w600),
      ),
    ),
  );
}

Widget namaToko() {
  return Padding(
    padding: const EdgeInsets.only(left: 5),
    child: Align(
      alignment: Alignment.centerLeft,
      child: Text("Bengkel VM MOTOR",
          style: TextStyle(
              color: Colors.black, fontSize: 12, fontWeight: FontWeight.w800)),
    ),
  );
}

Widget alamatToko() {
  return Padding(
    padding: const EdgeInsets.only(left: 5),
    child: Align(
      alignment: Alignment.centerLeft,
      child: Text("Jl. Soekarno Hatta Km.5 Balikpapan"),
    ),
  );
}

Widget jarakToko() {
  return Padding(
    padding: const EdgeInsets.only(left: 5),
    child: Align(
      alignment: Alignment.centerLeft,
      child: Text("Jarak 2KM dari lokasi anda",
          style: TextStyle(
              color: Colors.grey, fontSize: 12, fontWeight: FontWeight.w800)),
    ),
  );
}

Widget linkMapToko() {
  return Padding(
    padding: const EdgeInsets.only(left: 5),
    child: Align(
      alignment: Alignment.centerLeft,
      child: Text("lihat di Google Maps",
          style: TextStyle(
              color: Colors.blue, fontSize: 12, fontWeight: FontWeight.w800)),
    ),
  );
}
