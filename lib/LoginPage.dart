import 'package:flutter/material.dart';
import 'package:otobenk/DashboardPage.dart';
import 'package:otobenk/OnBoardingPage.dart';
import 'package:otobenk/model/LoginModel.dart';
import 'package:otobenk/RegisterPage.dart';
import 'package:otobenk/model/SessionModel.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _formKey = new GlobalKey<FormState>();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  LoginModel loginModel = null;

  String email = '';
  String password = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        // padding: const EdgeInsets.symmetric(horizontal: 43.0),
        // padding: const EdgeInsets.only(left: 43.0, right: 43.0, top: 100.0),
        margin:
            EdgeInsets.only(top: 10.0, bottom: 50.0, right: 16.0, left: 16.0),
        child: Form(
          child: Container(
            alignment: Alignment.center,
            child: SingleChildScrollView(
              child: Column(
                // mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Image.asset(
                    'assets/images/logo_otobenk.png',
                    width: 100,
                  ),
                  SizedBox(
                    height: 50,
                  ),
                  Text(
                    "Selamat Datang",
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.w700),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  _email(),
                  SizedBox(
                    height: 20,
                  ),
                  _password(),
                  SizedBox(
                    height: 30,
                  ),
                  _daftarButton(context),
                  SizedBox(
                    height: 10,
                  ),
                  Text("Belum Punya Akun?"),
                  // Text((loginModel != null)
                  //     ? "ada akun dengan email ${loginModel.email}"
                  //     : "tidak ditemukan akun"),
                  SizedBox(
                    height: 5,
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => RegisterPage()));
                    },
                    child: new Text(
                      "Daftar Disini",
                      style: TextStyle(color: Color.fromRGBO(47, 214, 250, 10)),
                    ),
                  )
                ],
              ),
            ),
          ),
          key: _formKey,
        ),
      ),
    );
  }

  InputDecoration _buildInputDecoration(String hint, String iconPath) {
    return InputDecoration(
        focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Color.fromRGBO(0, 0, 0, 1))),
        hintText: hint,
        enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Color.fromRGBO(151, 151, 151, 1))),
        hintStyle: TextStyle(color: Color.fromRGBO(0, 0, 0, 1)),
        icon: iconPath != ''
            ? Image.asset(
                iconPath,
                width: 20,
                color: Color.fromRGBO(0, 0, 0, 10),
              )
            : null,
        errorStyle: TextStyle(color: Color.fromRGBO(248, 218, 87, 1)),
        errorBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Color.fromRGBO(248, 218, 87, 1))),
        focusedErrorBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Color.fromRGBO(248, 218, 87, 1))));
  }

  Widget _email() {
    return TextFormField(
      controller: emailController,
      validator: (value) =>
          isEmail(value) ? "Maaf, kami tidak mengenali email anda." : null,
      style: TextStyle(
          color: Color.fromRGBO(0, 0, 0, 1), fontFamily: 'RadikalLight'),
      decoration:
          _buildInputDecoration("Email", 'assets/icons/ic_paper-plane.png'),
      onSaved: (String value) {
        print(value.toString());
        email = value.toString();
      },
    );
  }

  Widget _password() {
    return TextFormField(
      controller: passwordController,
      validator: (value) =>
          value.isEmpty ? "Password anda tidak boleh dikosongkan" : null,
      style: TextStyle(
          color: Color.fromRGBO(0, 0, 0, 1), fontFamily: 'RadikalLight'),
      decoration: _buildInputDecoration("Password", 'assets/icons/ic_link.png'),
      onSaved: (String value) {
        password = value;
      },
    );
  }

  Widget _daftarButton(BuildContext context) {
    return Container(
      // margin: EdgeInsets.only(top: 43.0),
      width: MediaQuery.of(context).size.width * 0.62,
      child: SizedBox(
        width: 10.0,
        height: 50.0,
        child: RaisedButton(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(25.0),
              side: BorderSide(color: Color.fromRGBO(47, 214, 250, 10))),
          child: const Text(
            "Masuk",
            // style: TextStyle(color: Color.fromRGBO(47, 214, 250, 10)),
            style: TextStyle(
                color: Color.fromRGBO(252, 252, 252, 10),
                fontFamily: 'RadikalMedium',
                fontSize: 16),
          ),
          color: Color.fromRGBO(47, 214, 250, 10),
          // elevation: 4.0,
          onPressed: () {
            // print(password);
            LoginModel.connectToData(
                    emailController.text, passwordController.text)
                .then((value) {
              loginModel = value;
              SessionModel.save(value.id.toString());

              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => DashboardPage()));
              setState(() {});
            });
          },
        ),
      ),
    );
  }

  bool isEmail(String value) {
    String regex =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';

    RegExp regExp = new RegExp(regex);
    return value.isNotEmpty && regExp.hasMatch(value);
  }
}
