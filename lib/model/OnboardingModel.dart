class OnboardingModel {
  String image;
  String title;
  String description;

  OnboardingModel({this.image, this.title, this.description});
}
