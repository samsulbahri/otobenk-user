import 'dart:convert';
import 'dart:io';
import 'dart:developer';

import 'package:http/http.dart' as http;

class LoginModel {
  String id;

  LoginModel({this.id});

  factory LoginModel.check(Map<String, dynamic> obj) {
    return LoginModel(id: obj['data']['id_user']);
  }

  static Future<LoginModel> connectToData(String email, String password) async {
    String url = "http://api.otobenk.com/api/user/login";

    Map<String, String> headerOtobenk = {
      'Content-type': 'application/x-www-form-urlencoded',
      'Accept': 'application/json',
      'apikey': 'otobenk'
    };
    var res = await http.post(url,
        headers: headerOtobenk, body: {"email": email, "password": password});
    var jsonObject = json.decode(res.body);

    return LoginModel.check(jsonObject);
  }
}
